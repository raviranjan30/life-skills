# Listening and Active Communication

## Question 1: What are the steps/strategies to do Active Listening? (Minimum 6 points)

 Steps for Active Listening:

* pay all your attention to the speaker.
* Don't get distracted while listening.
* Paraphrase the topic to check if you both are the same page.
* Don't interrupt until the speaker finishes.
* Use door openers to keep the person talking and show interest.
* If appropriate take notes.
* Show that you are listening by using body language.

## Question 2: According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

Reflection Listening is a subset of Active Listening with more focus put on emotional side of the speaker which includes watching body language, emotions and expressions.

While listening to the speaker showing or expressing genuine interest to the thing speaker is sharing. This will make the speaker feel good and could help him express his thoughts even more.

Even if there are no suggestions, we can always respond with the same thing to ensure the speaker that this is what you have noticed. Sometimes the speaker just want someone to listen to them and are not expecting any suggestions.

Using empathy while listening could help both the speaker and the listener for better understanding of the topic being discussed.

## Question 3: What are the obstacles in your listening process?

These are the obstacles i have while listening to other people:

* Getting distracted if something is already on my mind.
* Sometimes i interrupt in between if i think i will forget to talk about this later.

## Question 4: What can you do to improve your listening?

The first thing i will do to improve my listening is to remove these two obstacle which i mentioned above which is getting distracted sometimes and interrupting.

I will use Reflective Listening to encourage the speaker to share his/her thoughts more without any hesitation. This will also make the speaker feel good and relaxed.

## Question 5: When do you switch to Passive communication style in your day to day life?

When i talk to someone i admire or care.

## Question 6: When do you switch into Aggressive communication styles in your day to day life?

When someone tries to make me feel bad intentionally. Or when someone makes fun of me for no reason just to look cool in front of other people.

## Question 7: When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

When i really want the other person to understand and apply some logic or sense. When i want them to question themselves for something they do or did.

## Question 8: How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life? (Watch the videos first before answering this question.)

These are the things which i would like to mention for making my communication more assertive :

* Try to list my needs and feelings to the other person.
* Try to know what other person wants and what his/her needs are.
* Valuing other's needs over my needs only if it is necessary as long as it wont become a habit or a part of relationship itself.
* Try not to be Aggressive.
* Try not to be Passive as well.
* Understanding both my needs as well as other person's need and then figuring out what is best for both of us.
