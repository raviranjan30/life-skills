# Prevention of Sexual Harassment

## What kinds of behavior cause sexual harassment?

Behavior that includes any unwanted or inappropriate behavior of a sexual nature. These could be of 3 kinds :

* Verbal: Making sexual comments, jokes, or propositions.
* Visual: Showing explicit images, making sexual gestures, or staring inappropriately.
* Physical: Unwanted touching, hugging, or blocking someone's way.

## What would you do in case you face or witness any incident or repeated incidents of such behavior?

### Steps to Take if You Face or Witness Sexual Harassment

#### 1.Speak Up

* Tell the person that their behavior is inappropriate and must stop.
* Make it clear that their actions are unwelcome.

#### 2. Document Everything

* Record the incidents if possible, or any witness.
* save any texts, or emails if someone is asking for sexual favours.

#### 3. Report to any manager or HR who can look into this matter
