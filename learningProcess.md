# Learning Process

## Question 1: What is the Feynman Technique? Explain in 1 line.

 Feynman Technique is all about explaining the thing to yourself or others which you have learned to test the depth of the knowledge you gained over that topic.

## Question 2 : In this video, what was the most interesting story or idea for you ?

The most interesting part of this video was when she talked about the focused mind and the diffuse mind. This was interesting for me because I have experienced this myself but never knew it works the same for everyone else. Now that I know this is actually a good thing to do, I can use it more often.
The part when she talked about we should always recall what we have learned is the best thing to train your brain was actually impressive, i do it sometimes but now i can do it all the time to keep my brain hooked to that particular topic.

## Question 3: What are active and diffused modes of thinking?

Active modes of thinking occur when we try to learn, understand, or think about something with full focus in order to keep our mind away from other thoughts or distractions.
The diffuse mode of thinking, on the other hand, is when you let your brain work without actively focusing on the task. Sometimes our brain figures out solutions even when we are not consciously thinking about the problem, and then suddenly, we get the idea we needed.
Overall, if we get stuck while in the active mode of thinking, we should take a break sometimes, as relaxation is a part of the learning process itself.

## Question 4: According to the video, what are the steps to take when approaching a new topic? Only mention the points.

Steps to keep in mind when approaching a new topic:

* Deconstruct the skill into smaller steps.
* learn enough to self-correct.
* Remove practice barriers.
* practice for at least 20 hours.

## Question 5: What are some of the actions you can take going forward to improve your learning process?

Actions i might take going forward to improve my learning process are:

* Recall the topics at least a couple fo time with proper intervals.
* Try to ask questions to myself about why, how or when.
* Divide my task or topic into smaller parts to get along with the topic which will be beneficial as i won't feel stupid right in the beginning.
* practicing more to learn it enough so i could explain it to anyone anytime.
* while learning something, try to discuss about the same topic with other people to get better at it.
