
# Messaging Queues

It is an architectural technique to break applications apart with asynchronous communication.
Messaging Queues can be broken down into two parts:

* Message: It simply means a piece of information or data that needs to be processed from one system to another system.
* Queue: It refers to a line of things that are processed in sequence.

So we can think of a message queue as a queue that holds a number of messages in the same order they are received and delivers them.

## What are Messaging Queues?

Messaging queues are like virtual lines where you can put messages (pieces of information or data) to be processed later. Imagine you're at a post office. You drop your letter into the mailbox, and it gets processed and delivered to the recipient eventually. The mailbox is like a queue, and your letter is a message.

Similarly, in the world of software, messaging queues work similarly. You have systems that need to communicate with each other, but they don't have to do it immediately. Instead, they can put messages into a queue, and other systems can pick up those messages and process them when they're ready.

## Why they are used?

Message queues are essential for several reasons, as they provide communication and coordination between distributed applications. Moreover, they can significantly simplify the coding of decoupled applications while enhancing reliability, performance, and scalability.

Some Benefits of Message Queue are :

* Better Performance
* Increased Reliability
* Granular Scalability
* Simplified Decoupling

## What are popular tools?

### Popular Tools for Message Queues

There are several tools and technologies that have emerged to facilitate the implementation of message queues in various use cases. Here are some popular tools for message queues:

#### 1. Apache Kafka

Apache Kafka is well-suited for use cases such as event sourcing, log aggregation, and stream processing.

#### 2. RabbitMQ

RabbitMQ is a robust and highly reliable message broker that implements the Advanced Message Queuing Protocol (AMQP). RabbitMQ is known for its flexibility, ease of use, and extensive support for programming languages and frameworks. It supports various messaging patterns.

#### 3. Amazon SQS (Simple Queue Service)

Amazon SQS is provided by Amazon Web Services (AWS). Amazon SQS eliminates the operational overhead of managing message queues, allowing developers to focus on building applications.

#### 4. Microsoft Azure Service Bus

Microsoft Azure Service Bus is a fully managed messaging service offered by Microsoft Azure for building cloud-based applications. It provides support for both queuing and publish/subscribe messaging patterns, as well as features such as message ordering, sessions, and dead-lettering.

#### 5. Google Cloud Pub/Sub

Google Cloud Pub/Sub is a globally distributed message queuing service provided by Google Cloud Platform (GCP).Google Cloud Pub/Sub supports high-throughput message ingestion and delivery, as well as features such as message ordering, retention, and push/pull subscriptions.

## What is Enterprise Message Bus?

Enterprise Message Bus (EMB) is like a central messaging system for a company's computer systems. It's a way for different software applications and systems within a company to communicate with each other efficiently and reliably.

Imagine a busy city with many different modes of transportation: cars, buses, trains, and planes. Each mode has its own routes and schedules, but they all need a way to coordinate and share information to keep the city running smoothly.

Similarly, in a company, there are different software systems and applications like sales, inventory, and customer service that need to share information and work together. An Enterprise Message Bus acts as the central hub where messages can be sent and received between these different systems.

The EMB ensures that messages are delivered to the right place at the right time, even if the systems are using different technologies or languages. It helps streamline communication, improve efficiency, and reduce errors across the organization.

## References

* [What is a Message Queue](https://www.youtube.com/watch?v=xErwDaOc-Gs)
* [Benefits of Message Queues](https://aws.amazon.com/message-queue/benefits/)
* [Popular Message Queue Tools](https://www.g2.com/categories/message-queue-mq)
* [Enterprise Service Bus](https://www.youtube.com/watch?v=VHzWswQNtgk)
