# Grit and Growth Mindset

## Question 1: Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words

The topic of this video is about mainly about why people succeed in life and the idea is Grit which is basically the passion one should have for very long term goals, it needs stamina and working not just for weeks or months but for years to accomplish his/her dream. The best way to become grittier we should have Growth-mindset which is basically  the belief that the ability to learn is not fixed and it change with your efforts.

## Question 2: Paraphrase (summarize) the video in a few (1 or 2) lines in your own words

Topic of this video was about Growth-mindset which simply means having the belief in your capacity to learn and grow and Why some people succeed while people who are equally talented, do not. There are mostly two types of mindset which are Fixed mindset and Growth mindset.
Fixed mindset believes that you are not in control of your abilities and Growth mindset thinks skills and Intelligence are grown and developed, or simple that you are in control of your abilities.

## Question 3: What is the Internal Locus of Control? What is the key point in the video?

The degree to which you believe you have control over your life is called as Locus of control
Key Points in this video was:

* External Locus of control, believing that factors you could not control are the reason you did well. Such as being gifted or smart etc.
* Internal Locus of control, believing that the factors you can control led to their outcomes. Such as hard work.
* The key of being motivated is through Internal Locus of control, believing you have control over your life and things happening to you are because of what you do in your life.
* Best way to gain Internal Locus of control is through taking some time and appreciating what you did and believing that it did happen because of your hard work.

## Question 4: What are the key points mentioned by speaker to build growth mindset (explanation not needed)

1. Having a strong believe in yourself, that you could figure out things on your own.
2. Stop having negative thoughts about questioning your capability.
3. Identify your goal and make a plan on how to do it.
4. Recognize, Accept, and even Embrace the challenges and difficulties you face in the your path to grow.

## Question 5: What are your ideas to take action and build Growth Mindset?

I think believing in yourself and then having consistency could do wonders in your life.
To build a Growth mindset we must first achieve a sense of Internal Locus of control which could start making us focus more on what we do rather than focusing on what went wrong which was not in our control. Once we achieve this we can combine this with the idea of believing in yourself which could make us do things on our own and never wanting others to do it for us. Also appreciating yourself everyday for keep moving will maintain the motivation to do it everyday.
